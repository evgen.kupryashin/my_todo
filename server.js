const express = require("express");
const kenx = require("knex");

const db = kenx({
  client: "pg",
  connection: {
    host: "127.0.0.1",
    user: "todo_user",
    password: "actpro-123",
    database: "todo_db"
  }
});

const app = express();
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.set("view engine", "ejs");
app.use(express.static("public"));

// res.render
app.get("/", (req, res) => {
  db.select("*")
    .from("task")
    .then(data => {
      res.render("index", { todos: data });
    })
    .catch(err => res.status(400).json(err));
});

// create new task
app.post("/addTask", (req, res) => {
  const { textTodo } = req.body;
  db("task")
    .insert({ task: textTodo })
    .returning("*")
    .then(todo => {
      res.redirect("/");
    })
    .catch(err => {
      res.status(400).json({ message: "unable to create a new task" });
    });
});

// Toggle task status (from todo to done and vice versa)
app.put("/toggleTaskStatus/:id", (req, res) => {
  const { id } = req.params;

  db("task")
    .where("id", "=", id)
    .increment("status", 1) // Increment status by 1 (0 to 1 or 1 to 2, etc.)
    .returning("status")
    .then(status => {
      res.json({ status: status[0] });
    })
    .catch(err => {
      res.status(400).json({ message: "Unable to toggle task status" });
    });
});

app.listen(8080, () => console.log("app is running on port 8080"));
